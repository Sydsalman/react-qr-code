import React, {Component} from 'react';
import './App.css';
import { Container,Row, Col} from 'react-bootstrap';

class App extends Component {
  constructor(props){
    super(props);
    
    this.state = {
      type: 'single'
    }
    this.handleTypeChange = this.handleTypeChange.bind(this);
    this.handleReset = this.handleReset.bind(this);
  }
  handleTypeChange(e){
    this.setState({
      type: e.target.value
    });
    if(e.target.value==='single'){
      document.getElementById('myForm').setAttribute('encType','application/x-www-form-urlencoded')
    }
    else{
      document.getElementById('myForm').setAttribute('encType','multipart/form-data')
    }
  }
  handleReset(e){
    e.preventDefault();
    this.setState({
      type:'single'
    },()=>{
      document.getElementById('myForm').setAttribute('encType','application/x-www-form-urlencoded');
      document.getElementById('email').value='';
      document.getElementById('subject').value='';
      document.getElementById('body').value='';
    });
  }
 
  render(){
    const type = this.state.type;
  return (
    <Container>
      <Row>
        <Col sm md={{span:'8',offset:'2'}} lg={{span:'8',offset:'2'}} className='cont'>
          <form id="myForm" onReset={this.handleReset} method="POST" action="http://localhost:8081/getqr" encType="application/x-www-form-urlencoded">
                          <Row className="mb-3 mt-4 ">
                            <Col md="1">
                                <label className="form-label">Type</label>
                            </Col>
                            <Col className="mr-2" md="10">
                              <Row>
                                <Col  md="6">
                                        <input type="radio"
                                         id="single" 
                                         name="type" 
                                         value="single" 
                                         checked={type === 'single'} 
                                         onChange={this.handleTypeChange}
                                         />
                                        <label htmlFor="single" className='ml-2'>Single</label>
                                </Col>
                                <Col md="6">                               
                                    <input type="radio"
                                     id="multiple" 
                                     name="type" 
                                     value="multiple" 
                                     checked={type === 'multiple'}
                                     onChange={this.handleTypeChange}/>
                                    <label htmlFor="multiple" className='ml-2'>Multiple</label>
                                </Col>
                              </Row>
                            </Col>                            
                        </Row>
                        { type==='single' ?
                             <div className="mb-3 singleUpload">
                              <label htmlFor="email" className="form-label">Email Id</label>
                              <input type="email"
                               className="form-control" 
                               id="email" 
                               name="email" 
                               placeholder="Enter Email ID"
                               required  />
                            </div>
                          :
                            <div className="mb-3 bulkUpload">
                              <label htmlFor="sendEmail" className="form-label">To</label>
                              <input type="file"
                               className="form-control" 
                               id="sendEmail" 
                               name="mailList" 
                               accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" 
                               placeholder="Please select a File"
                               />
                            </div>
                        }
                     
                     
                        <div className="mb-3">
                            <label htmlFor="subject" 
                            className="form-label">Subject</label>
                            <input type="text" 
                            className="form-control"
                            id="subject" 
                            name="subjectText" 
                            placeholder="Enter Subject of the Email"
                            />
                        </div>
                        <div className="mb-3">
                            <label htmlFor="body" className="form-label">Body</label>
                            <textarea rows="4"
                             cols="50" 
                             className="form-control" 
                             id="body" 
                             name="bodyText" 
                             maxLength={1000} 
                             placeholder="Enter Body of the Email"></textarea>
                        </div>
                        <button type="submit" className="btn btn-primary mr-3 mb-3" >Generate QR Code</button>
                        <button type="reset" className="btn btn-warning mb-3">Reset</button>
          </form>
        </Col>
      </Row>
    </Container>
  );
  }
}

export default App;
